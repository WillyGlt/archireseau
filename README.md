# `Documentation`

### William Gillot - Louis Maury - Thomas Bellamy

## `Sommaire`

- [`Documentation`](#documentation)
  - [`Sommaire`](#sommaire)
  - [`Dépôt Git`](#dépôt-git)
  - [`Etude de l'existant`](#etude-de-lexistant)
  - [`Solution Proposée`:](#solution-proposée)
  - [`Architecture Globale`](#architecture-globale)
    - [`Bâtiment A`](#bâtiment-a)
    - [`Bâtiment B`](#bâtiment-b)
    - [`Bâtiment B-Bis`](#bâtiment-b-bis)
    - [`Bâtiment C`](#bâtiment-c)
    - [`Bâtiment D`](#bâtiment-d)
  - [`Détails`](#détails)
    - [`Vlans`](#vlans)
    - [`Spanning Tree`](#spanning-tree)
    - [`LACP LAG`](#lacp-lag)
    - [`Firewall`](#firewall)
  - [`VPN et Sécurité`](#vpn-et-sécurité)
    - [`VPN`](#vpn)
      - [`IPsec`](#ipsec)
      - [`SSL`](#ssl)
    - [`Sécurité`](#sécurité)
      - [`Profil de sécurité`](#profil-de-sécurité)
      - [`Sécurité Mail`](#sécurité-mail)
  - [`Annexe de Commandes`](#annexe-de-commandes)
    - [`Switch`](#switch)
      - [`Vlan`](#vlan)
      - [`Trunk Exemple`](#trunk-exemple)
      - [`Access Exemple`](#access-exemple)
      - [`VTP Liste Commande`](#vtp-liste-commande)
      - [`STP Liste Commande`](#stp-liste-commande)
      - [`Agregation de liens Liste Commande`](#agregation-de-liens-liste-commande)
    - [`Routeur`](#routeur)
      - [`Exemple de Routage Inter-Vlan`](#exemple-de-routage-inter-vlan)
    - [`VPN et Sécurité`](#vpn-et-sécurité-1)
      - [`Exemple de configuration de Sécurité pour le VPN`](#exemple-de-configuration-de-sécurité-pour-le-vpn)
      - [`Exemple de configuration VPN IPsec`](#exemple-de-configuration-vpn-ipsec)
      - [`Exemple de configuration VPN SSL`](#exemple-de-configuration-vpn-ssl)

## `Dépôt Git`
[Lien de notre dépôt git](https://gitlab.com/WillyGlt/archireseau.git)

## `Etude de l'existant`
![Archi Départ](Images/archidepart.png)

D’après ce schéma et les différents échanges avec le client nous constatons :

  • Une absence de redondance des liens 

  • Une cascade de commutateurs qui participent au ralentissement du réseau 

  • L’absence de sécurité réseau (firewall)

  • La partie téléphonie peut être modifier afin de décharger les switch.

  • Absence de segmentations réseaux 

## `Solution Proposée`:

## `Architecture Globale`
![Archi Globale](Images/archiglobale.png)

### `Bâtiment A`
![Archi Bat A](Images/batA.png)

### `Bâtiment B`
![Archi Bat B](Images/batB.png)

### `Bâtiment B-Bis`
![Archi Bat BBis](Images/batBBis.png)

### `Bâtiment C`
![Archi Bat C](Images/batC.png)

### `Bâtiment D`
![Archi Bat D](Images/batD.png)

## `Détails`

Nous notons la présence d'un Datacenter dans le Bâtiment A ainsi que votre demande d'ajout d'un Datacenter dans le bâtiment C, nous avons donc prévu l'architecture avec ce dernier en vous proposant quelques modifications afin d'optimiser votre architecture existante.

Afin d’alléger un maximum les liaisons nous vous recommandons de mettre en place plusieurs serveurs virtualisés, ce qui a pour avantage de pouvoir délivrer plusieurs services sur une seule machine, l’inconvéniant est que si une machine physique vient à être défectueuse tous les services tomberont. C’est pourquoi nous allons les redonder. Nous vous précisons ci-dessous les services qui pourront être déployés selon votre convenance (La particularité de tous ces services est d’éviter un maximum les requêtes des utilisateurs vers internet):

  • Le Serveur d’impression, il permet de déployer les drivers d’impressions directement au client et d’éviter tout problème de configuration d’imprimante, mais aussi de centraliser les documents à imprimer, de gérer les droits des utilisateurs.

  • Le serveur WEB, il permet de centraliser les requêtes des utilisateurs et de garder en mémoire les sites fréquemment utilisés afin d’éviter des requêtes inutiles vers internet.

  • Un serveur DHCP, il permet d’adresser de manière dynamique les adresses IP aux stations clientes.

  • Un serveur DNS, il permet de garder en mémoire des liaisons d’adresses IP et d’URL.

Nous vous conseillons l'ajout d'imprimantes dans chaque bâtiment (et d'une deuxième pour l'extension B-Bis) ainsi que d'un serveur d'impression au niveau du bâtiment A pour la gestion de priorité.

Nous vous avons également proposé une architecture pour votre nouveau bâtiment (D) avec la mise en place de 50 téléphones et 50 PCs répartis en 2 fois 25, ainsi qu'une imprimante et un routeur pour gérer la liaison WAN.

Pour la nouvelle extension du bâtiment B (B-Bis) nous avons mis en place 2 fois 48 téléphones reliés aux PCs (96 en tout) ainsi qu'une imprimante. Nous avons également décidé de mettre en place une redondance au niveau des Switchs de cette nouvelle extension en mettant en place du spanning tree.

Suite à l'acquisition de votre nouvelle société, nous vous proposons la mise en place de FireWall afin de sécuriser l'ensemble de votre réseau pour garantir la bonne continuité d'activité (nous vous détaillons cela plus bas).

Pour une meilleure gestion des liaisons WAN entre les bâtiments, nous vous proposons d'intégrer du SD-WAN au WAN existant, rendu possible grâce à des FireWalls Fortinet par exemple (ou Palo Alto).
Nous vous conseillons de garder la configuration Vlan que nous vous avons soumise pour votre premier bâtiment ainsi que la mise en place de Spanning Tree et de LACP/LAG.

### `Vlans`

Nous vous conseillons de créer 6 Vlans différents:

Un Vlan datacenter pour qu'ils puissent communiquer entre eux.

Un Vlan Pc pour la même raison, nous aurions pu les mettre dans des Vlan selon les bâtiments mais nous avons préféré centraliser notre archi.

Un Vlan pour les imprimantes.

Un Vlan pour le serveur Impression.

Un Vlan pour les téléphones afin de finir de compartimenter notre archi.

Un Vlan Admin avec une Ip sur tous les Switchs pour pouvoir s'y connecter depuis du SSH par exemple.

### `Spanning Tree`

Nous vous proposons de mettre en place un Spanning Tree avec les configurations ci-dessous:

Activer le mode rapid STP sur tous les switch.

Switch A en mode Root Primary -> fa0/1 et fa0/2 en priorité.

Switch B en mode Root Secondary.

Lien coupé entre Switch B et C.

Switch A-B-C-D haute priorité.

Switch 2B-2C basse priorité.

Switch Bbis haute priorité.

Lien coupé entre Switch BBis et Bbis2.

Activer le Portfast sur les fa en lien avec les pc, imprimantes, téléphones et serveurs.

Mise en place des BPDU Filter sur les mêmes ports qu'au dessus (les terminaux).

### `LACP LAG`

Doubler les cables fibres entre les bat A-B-C avec du LACP.

Mise en place de quatres câbles entre le switch 1B et 1Bbis en LACP.

Doubler les cables dans les batiments au niveau des switchs et des serveurs avec du LAG.

### `Firewall`

![Schéma FW](Images/schemaFW.png)

Pour votre premier bâtiment, nous vous conseillons l'ajout d'un firewall de type ISFW (Internal Segmentation FireWall) en coeur de réseau afin d'amener un niveau de sécurité EST/OUEST ainsi qu'un firewall NGFW (Next Generation FireWall) au dessus du coeur de réseau pour gérer les communications intra-zones (par le WAN) ainsi qu'une protection NORD/SUD.
Pour une meilleur gestion, communication et sécurité, nous vous proposons de mettre en place un NGFW au niveau du bâtiment D pour les mêmes raisons.

Suite à votre acquisition, nous vous recommandons deux nouveaux types de Firewall en plus d'un d'un NGFW pour apporter une sécurité et permettre une communication inter-bâtiment depuis le WAN:

  • Un Firewall Industriel pour vos sites industriels afin d'apporter une sécurité supplémentaire et garantir votre activité.

  • Un DCFW (DataCenter Firewall) au niveau de votre siège car nous avons noté (suite à notre échange) la présence d'une ferme de Datacenter et ce dernier est spécifique pour cette situation.

## `VPN et Sécurité`

### `VPN`
#### `IPsec`
Le VPN IPsec (Virtual Private Network, Internet Protocol Security) permet de connecter tous vos sites entre eux de manière sécurisée (on parle de VPN site à site), c'est pourquoi nous vous recommandons la mise en place de cette technologie.

Vous trouverez ci dessous les différentes étapes qui permettront de le créer sur vos Next Generation Firewall.

#### `SSL`
Le VPN SSL (Virtual Private Network, Secure Socket Layer) permet quant à lui de créer une connexion distante par le biais du naviguateur web (HTTPS) ou client lourd. Cette technologie peut permettre à vos collaborateurs de se connecter depuis chez eux, pour le télétravail qui est devenu une actualitée avec la crise du Corona Virus, mais également une gestion à distance pour vos administrateurs.

Comme le VPN IPsec, nous vous avons également proposer une configuration ci dessous.
### `Sécurité`
#### `Profil de sécurité`
Afin de sécuriser votre infrastructure ainsi que vous permettre une meilleure gestion des terminaux présent sur vos sites, nous vous recommandons la mise en place de profil de sécurité comme l'Antivirus, le Web Filtering ou le DNS Filtering qui assurent un contrôle de vos applications sur vos machines comme la mise en liste noire de site malveillant par exemple ou l'accès à des plateformes qui n'ont pas de rapport avec votre secteur d'activité tel que les réseaux sociaux ou les jeux vidéos.

Nous vous avons proposé un exemple de configuration que nous pourrons modifier celon votre convenance pour laisser plus ou moins de liberté à vos collaborateurs.

#### `Sécurité Mail`
Les failles de sécurité dans les emails sont un problème qui à un énorme impact aujourd'hui, cela représente 91% des attaques dans le monde, c'est pourquoi nous vous proposons de mettre en place une sécurité supplémentaire à ce niveau à l'aide de la compagnie française Vade Secure avec laquelle nous sommes partenaires depuis quelques années.

Vade Secure permet une gestion et sécurisation de vos émails très rapidement et simplement sur vos appareils, cette solution ce base sur une intelligence artificielle afin de prévenir et vous protéger contre le Spam, les Malwares, le fishing ainsi que les arnaques au Président.

Cette entreprise propose également une formation/sensibilisation pour vos employés qui sont la dernière rempart contre ce genre d'attaque.

[Site Vade Secure](https://www.vadesecure.com/fr/)

## `Annexe de Commandes`

### `Switch`

#### `Vlan`
```
enable 
conf t
vlan 10
name vlan_admin
ex
vlan 20
name vlan_pc
ex
vlan 30
name vlan_server
ex
vlan 40
name vlan_server_printer
ex
vlan 50
name vlan_printer
ex
vlan 60
name vlan_phone

interface vlan 10
ip address 10.10.0.1 255.255.255.0
no shut
ex
ip default-gateway 10.10.0.254
```

#### `Trunk Exemple`
```
interface fastEthernet 0/2
switchport mode trunk
switchport trunk allowed vlan 1,10,20,30
no shut
ex
```

#### `Access Exemple`
```
interface fastEthernet 0/3
switchport mode access 
no shut
ex
```

#### `VTP Liste Commande`
Switch server (Un seul autorisé)
```
vtp mode server
```
Switch client
```
vtp mode client
```
Switch liaison
```
vtp mode transparent
```
VTP désactivé
```
vtp mode off
```
Mise en place du domain VTP avec mdp
```
vtp domain mydomain
vtp password mypassword
```

#### `STP Liste Commande`
Activer le Rapid PVST
```
spanning-tree mode rapid-pvst
```
Attribution root sur un switch
```
spanning-tree vlan 1-100 root primary
end
```
Attribution des prioritées sur un switch (0-61440)
```
spanning-tree priority 0
```
Attribution des prioritées sur un port (16-240)
```
int fastEthernet 0/1 
spanning-tree vlan 1 port-priority 16
```
Création de Portfast
```
int fa0/1
spanning-tree portfast
end
```

#### `Agregation de liens Liste Commande`
Différents Mode
```
  active     Enable LACP unconditionally
  auto       Enable PAgP only if a PAgP device is detected
  desirable  Enable PAgP unconditionally
  on         Enable Etherchannel only
  passive    Enable LACP only if a LACP device is detected
```
Mise en place Exemple
```
interface range fastEthernet 0/1 - 2
channel-group 1 mode active
ex
```

### `Routeur`

#### `Exemple de Routage Inter-Vlan`
```
# Routage inter-vlan coté Switch
interface gigabitEthernet 0/0
no shutdown 
ex

# Vlan 10
interface gigabitEthernet 0/0.10
encapsulation dot1Q 10
ip address 10.10.0.254 255.255.255.0
no shutdown 
ex

# Vlan 20
interface gigabitEthernet 0/0.20
encapsulation dot1Q 20
no shut
ex

# Vlan 30
interface gigabitEthernet 0/0.30
encapsulation dot1Q 30
no shutdown 
ex

# Vlan 40
interface gigabitEthernet 0/0.40
encapsulation dot1Q 40
no shutdown 
ex

# Vlan 50
interface gigabitEthernet 0/0.50
encapsulation dot1Q 50
no shutdown 
ex

# Vlan 60
interface gigabitEthernet 0/0.60
encapsulation dot1Q 60
no shutdown 
ex
```
### `VPN et Sécurité`
#### `Exemple de configuration de Sécurité pour le VPN`

Pour la sécurité, nous allons créer un User et un Admin (avec une double authentification pour le profil administrateur):

![UserDefinition](Images/UserDefinition.png)

Ainsi que des Profils de sécurité:

Antivirus:

![Antivirus](Images/LTW-ac-global.png)

WebFiltering:

![WebFiltering](Images/LTW-wf-global.png)

![WebFiltering](Images/LTW-wf-1.png)

![WebFiltering](Images/LTW-wf-2.png)

![WebFiltering](Images/LTW-wf-3.png)

DNSFiltering:

![DNSFiltering](Images/LTW-df-global.png)

![DNSFiltering](Images/LTW-df-1.png)

![DNSFiltering](Images/LTW-df-2.png)

![DNSFiltering](Images/LTW-df-3.png)

Pour les autres profils, les configurations par défaut sont largement suffisantes.

#### `Exemple de configuration VPN IPsec`
Nous devons tout d'abord créer une route statique pour forcer les flux à passer par le tunnel.

![StaticRoute](Images/StaticRoute.png)

Mettre en place le SDWAN et le relier au VPN Hébergeur:

![SDWAN](Images/SDWANZones.png)

Pour le VPN IPsec, nous allons créer un VPN Hébergeur par exemple celui du Bâtiment A:

![VPNABC Hébergeur](Images/VPNABC.png)

Nous créons ensuite les VPN afin de se connecter:

![VPNABC](Images/VPNABCC.png)

![VPND](Images/VPND.png)

![VPN New Site](Images/VPNNewSite.png)

#### `Exemple de configuration VPN SSL`

Pour la mise en place du VPN SSL:

Nous configurons tout d'abord son moyen de connexion:

![VPNSSL](Images/SSLSettings.png)

Puis nous personnalisons en fonction du type d'utilisateur (User ou Admin):

![VPNSSL](Images/SSLUser.png)

![VPNSSL](Images/SSLAdmin.png)